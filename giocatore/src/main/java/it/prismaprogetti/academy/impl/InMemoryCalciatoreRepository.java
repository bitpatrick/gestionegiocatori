package it.prismaprogetti.academy.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.NumeroMaglia;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Calciatore;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.CalciatoreRepository;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Ruolo;

/**
 * InMemory = memorizza gli oggetti nella RAM
 * 
 * @author patri
 *
 */
public class InMemoryCalciatoreRepository implements CalciatoreRepository {

	private Map<String, Calciatore> calciatoreByCodice = new HashMap<>();
	private Map<NumeroMaglia, Calciatore> calciatoreByNumeroMaglia = new HashMap<>();
	private Map<Ruolo, List<Calciatore>> calciatoriByRuolo = new HashMap<>();
	private Set<Calciatore> calciatori = new HashSet<>();

	@Override
	public void insert(Calciatore calciatore) {

		this.calciatoreByCodice.put(calciatore.getCodice().getCodice(), calciatore);
		this.calciatoreByNumeroMaglia.put(calciatore.getNumeroMaglia(), calciatore);

		/*
		 * calciatori by Ruolo
		 */
		List<Calciatore> calciatoriByRuolo = this.calciatoriByRuolo.get(calciatore.getRuolo());
		if (calciatoriByRuolo == null) {
			calciatoriByRuolo = new ArrayList<>();
		}
		calciatoriByRuolo.add(calciatore);
		this.calciatoriByRuolo.put(calciatore.getRuolo(), calciatoriByRuolo);

		/*
		 * calciatori in memoria
		 */
		this.calciatori.add(calciatore);
	}

	@Override
	public Optional<Calciatore> getByCodice(String codice) {

		Optional<Calciatore> calciatoreOpt = Optional.ofNullable(this.calciatoreByCodice.get(codice));

		return calciatoreOpt;
	}

	@Override
	public Optional<Calciatore> getByNumeroMaglia(NumeroMaglia numeroMaglia) {

		Optional<Calciatore> calciatoreOpt = Optional.ofNullable(this.calciatoreByNumeroMaglia.get(numeroMaglia));

		return calciatoreOpt;
	}

	@Override
	public List<Calciatore> getByRuolo(Ruolo ruolo) {

		List<Calciatore> calciatori = this.calciatoriByRuolo.get(ruolo);

		return calciatori;
	}

	@Override
	public List<Calciatore> getAll() {

		return new ArrayList<>(Arrays.asList(this.calciatori.toArray(new Calciatore[this.calciatori.size()])));
	}

	@Override
	public void update(Calciatore calciatore) {

		this.calciatoreByCodice.put(calciatore.getCodice().getCodice(), calciatore);
		this.calciatoreByNumeroMaglia.put(calciatore.getNumeroMaglia(), calciatore);

		/*
		 * calciatori by Ruolo
		 */
		List<Calciatore> calciatoriByRuolo = this.calciatoriByRuolo.get(calciatore.getRuolo());
		for (int i = 0; i < calciatoriByRuolo.size(); i++) {

			if (calciatoriByRuolo.get(i).getCodice() == calciatore.getCodice()) {

				calciatoriByRuolo.remove(i);
				calciatoriByRuolo.add(calciatore);
			}
		}
		this.calciatoriByRuolo.put(calciatore.getRuolo(), calciatoriByRuolo);

	}

	@Override
	public void delete(Calciatore calciatore) {

		this.calciatoreByCodice.remove(calciatore.getCodice().getCodice());
		this.calciatoreByNumeroMaglia.remove(calciatore.getNumeroMaglia());
		this.calciatori.remove(calciatore);

		/*
		 * calciatori by Ruolo
		 */
		List<Calciatore> calciatoriByRuolo = this.calciatoriByRuolo.get(calciatore.getRuolo());
		calciatoriByRuolo.remove(calciatore);
		this.calciatoriByRuolo.put(calciatore.getRuolo(), calciatoriByRuolo);

	}

}
