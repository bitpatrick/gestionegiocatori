package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore;

public abstract class Giocatore {

	private Codice codice;
	private Nome nome;
	private Cognome cognome;
	private NumeroMaglia numeroMaglia;
	private Tipo tipo;

	protected Giocatore(Codice codice, Nome nome, Cognome cognome, NumeroMaglia numeroMaglia, Tipo tipo) {
		super();
		this.codice = codice;
		this.nome = nome;
		this.cognome = cognome;
		this.numeroMaglia = numeroMaglia;
		this.tipo = tipo;
	}

	public Codice getCodice() {
		return codice;
	}

	public Nome getNome() {
		return nome;
	}

	public Cognome getCognome() {
		return cognome;
	}

	public NumeroMaglia getNumeroMaglia() {
		return numeroMaglia;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setCodice(Codice codice) {
		this.codice = codice;
	}

	public void setNome(Nome nome) {
		this.nome = nome;
	}

	public void setCognome(Cognome cognome) {
		this.cognome = cognome;
	}

	public void setNumeroMaglia(NumeroMaglia numeroMaglia) {
		this.numeroMaglia = numeroMaglia;
	}

}
