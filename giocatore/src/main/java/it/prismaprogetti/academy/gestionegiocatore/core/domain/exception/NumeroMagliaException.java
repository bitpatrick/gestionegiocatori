package it.prismaprogetti.academy.gestionegiocatore.core.domain.exception;

public class NumeroMagliaException extends Exception {

	private NumeroMagliaException(String message) {
		super(message);
	}

	public static NumeroMagliaException numeroMagliaNonValido() {
		return new NumeroMagliaException("Il numero di maglia non � valido");
	}
	
	public static NumeroMagliaException numeroMagliaGi�Esistente() {
		return new NumeroMagliaException("Il numero di maglia � gi� esistente");
	}
	
	

}
