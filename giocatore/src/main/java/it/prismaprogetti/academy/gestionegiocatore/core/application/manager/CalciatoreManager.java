package it.prismaprogetti.academy.gestionegiocatore.core.application.manager;

import java.util.List;
import java.util.Optional;

import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.CalciatoreException;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.NumeroMagliaException;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Cognome;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Nome;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.NumeroMaglia;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Calciatore;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.CalciatoreRepository;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Ruolo;

public class CalciatoreManager {

	private final CalciatoreRepository calciatoreRepository;

	public CalciatoreManager(CalciatoreRepository calciatoreRepository) {
		super();
		this.calciatoreRepository = calciatoreRepository;
	}
	
	public List<Calciatore> recuperaCalciatori() {
		
		List<Calciatore> calciatori = calciatoreRepository.getAll();
		
		if ( calciatori == null || calciatori.isEmpty() ) {
			return null;
		}
		
		return calciatori;
	}

	public void inserisciNelRepository(Calciatore calciatore) throws CalciatoreException, NumeroMagliaException {

		/*
		 * verifica se il calciatore � gi� presente nel repository
		 */
		if (!calciatoreRepository.getByCodice(calciatore.getCodice().getCodice()).isEmpty()) {
			throw CalciatoreException.calciatoreGi�Esistente();
		}

		/*
		 * verifica se il numero di maglia scelto � gi� usato nel repository
		 */
		if (!calciatoreRepository.getByNumeroMaglia(calciatore.getNumeroMaglia()).isEmpty()) {
			throw NumeroMagliaException.numeroMagliaGi�Esistente();
		}

		calciatoreRepository.insert(calciatore);
		System.out.println(calciatore + " inserito con successo.");

	}

	public void modifica(Calciatore calciatore, Nome nome, Cognome cognome, NumeroMaglia numeroDiMaglia, Ruolo ruolo)
			throws CalciatoreException, NumeroMagliaException {

		if (calciatoreRepository.getByCodice(calciatore.getCodice().getCodice()).isEmpty()) {
			throw CalciatoreException.calciatoreNonPresente();
		}

		/*
		 * aggiorno dati anagrafici
		 */
		if( nome != null ) {
			calciatore.setNome(nome);
		}
		if ( cognome != null ) {
			calciatore.setCognome(cognome);
		}
		
		/*
		 * TENTO di aggiornare il numero di maglia
		 */
		if ( numeroDiMaglia != null ) {
			if (calciatoreRepository.getByNumeroMaglia(numeroDiMaglia).isPresent()) {
				throw NumeroMagliaException.numeroMagliaGi�Esistente();
			}
			calciatore.setNumeroMaglia(numeroDiMaglia);
		}
		
		/*
		 * aggiorno il ruolo del giocatore
		 */
		if ( ruolo != null ) {
			calciatore.setRuolo(ruolo);
		}
		
		calciatoreRepository.update(calciatore);

	}

	public Calciatore recuperaTramiteCodice(String codice) throws CalciatoreException {

		Optional<Calciatore> calciatore = calciatoreRepository.getByCodice(codice);
		return calciatore.orElseThrow(CalciatoreException::calciatoreNonPresente);

	}

	public List<Calciatore> recuperaCalciatoriTramiteRuolo(Ruolo ruolo) throws CalciatoreException {

		List<Calciatore> calciatori = calciatoreRepository.getByRuolo(ruolo);

		if (calciatori == null || calciatori.isEmpty()) {
			throw CalciatoreException.calciatoriNonPresenti();
		}

		return calciatori;

	}

	public void elimina(Calciatore calciatore) throws CalciatoreException {

		if (calciatoreRepository.getByCodice(calciatore.getCodice().getCodice()).isEmpty()) {
			CalciatoreException.calciatoreNonPresente();
		}

		calciatoreRepository.delete(calciatore);
		System.out.println(calciatore + " eliminato con successo");
	}

}
