package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore;

import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Calciatore;

public class Codice {

	private String codice;

	private Codice(String codice) {
		super();
		this.codice = codice;
	}

	public String getCodice() {
		return codice;
	}

	public static Codice creaCodice(Tipo tipoGiocatore) {

		String codice = "";

		switch (tipoGiocatore) {
		case CALCIATORE:

			int numeroCreazioneCalciatore = Calciatore.getNumeroCalciatoriGenerati() + 1; // 0 + 1

			codice += "C" + numeroCreazioneCalciatore; // C1

			break;

		default:
			throw new IllegalStateException("Non � stato inserito alcun Tipo di Giocatore");

		}

		return new Codice(codice);

	}

}
