package it.prismaprogetti.academy.gestionegiocatore.service;

import java.util.Scanner;

import it.prismaprogetti.academy.gestionegiocatore.core.application.manager.CalciatoreManager;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.CalciatoreException;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Calciatore;

public class MenuEliminaCalciatore {

	static {

		/*
		 * quando la classe viene carica in memoria allora viene eseguito questo blocco
		 */
		System.out.println("sono la classe menu elimina calciatore , sono stata caricata in memoria");

	}

	private static MenuEliminaCalciatore menuEliminaCalciatore;

	private MenuEliminaCalciatore() {
		super();
	}

	public static MenuEliminaCalciatore getMenuEliminaCalciatore() {
		if (menuEliminaCalciatore == null) {
			menuEliminaCalciatore = new MenuEliminaCalciatore();
		}
		return menuEliminaCalciatore;
	}

	public void eliminaCalciatore(Scanner input, CalciatoreManager calciatoreManager) {

		String parametro = null;
		Calciatore calciatore = null;

		do {

			System.out.println("inserisci codice calciatore che vuoi eliminare oppure N per tornare indietro");

			switch (parametro = input.nextLine().trim().toUpperCase()) {
			case "N":
				parametro = "N";
				break;

			default:

				try {
					calciatore = calciatoreManager.recuperaTramiteCodice(parametro);
					calciatoreManager.elimina(calciatore);
				} catch (CalciatoreException e) {
					System.err.println("Hai inserito un codice non valido");
					continue;
				}

				parametro = null;
				break;
			}

		} while (parametro == null);
	}

}
