package it.prismaprogetti.academy.gestionegiocatore.service;

import java.util.Scanner;

import it.prismaprogetti.academy.gestionegiocatore.core.application.manager.CalciatoreManager;

public class MenuPrincipale {

	private static MenuPrincipale menuPrincipale = null;
	private Scanner input = null;

	private MenuPrincipale() {
		super();
	}

	public static MenuPrincipale getMenuPrincipale() {

		if (menuPrincipale == null) {

			menuPrincipale = new MenuPrincipale();
		}

		return menuPrincipale;
	}

	public void avvio(CalciatoreManager calciatoreManager) {

		if (input == null) {
			input = new Scanner(System.in);
		}

		String parametro = null;
		do {

			System.out.println("Menu Principale");
			System.out.println("scegli 1 per menu gestione calciatori");
			System.out.println("scegli N per uscire");

			switch (parametro = input.nextLine().trim().toUpperCase()) {
			case "1":
				MenuGestioneCalciatore.getMenuGestioneCalciatore().avvio(input, calciatoreManager);
				break;

			case "N":
				parametro = "N";
				break;

			default:
				parametro = null;
				break;
			}
			
			

		} while (parametro == null);

		input.close();
		System.out.println("sei uscito dal programma");

	}

}
