package it.prismaprogetti.academy.gestionegiocatore.service;

import java.util.Scanner;

import it.prismaprogetti.academy.gestionegiocatore.core.application.manager.CalciatoreManager;

public class MenuGestioneCalciatore {

	private static MenuGestioneCalciatore menuGestioneCalciatore = null;

	private MenuGestioneCalciatore() {
		super();
	}

	public static MenuGestioneCalciatore getMenuGestioneCalciatore() {

		if (menuGestioneCalciatore == null) {

			menuGestioneCalciatore = new MenuGestioneCalciatore();
		}

		return menuGestioneCalciatore;

	}

	public void avvio(Scanner input, CalciatoreManager calciatoreManager) {

		String parametro = null;
		do {

			System.out.println("Menu Gestione Calciatore");
			System.out.println("1) inserisci");
			System.out.println("2) modifica");
			System.out.println("3) elimina");
			System.out.println("4) seleziona");
			System.out.println("5) menu precedente");

			switch (parametro = input.nextLine().trim().toUpperCase()) {
			case "1":

				MenuInserisciCalciatore.getMenuInserisciCalciatore().inserisciCalciatore(input, calciatoreManager);
				break;

			case "2":

				MenuModificaCalciatore.getMenuModificaCalciatore().avvio(input, calciatoreManager);
				break;

			case "3":

				MenuEliminaCalciatore.getMenuEliminaCalciatore().eliminaCalciatore(input, calciatoreManager);
				break;

			case "4":

				MenuSelezioneCalciatore.getMenuSelezioneCalciatore().avvio(input, calciatoreManager);
				break;

			case "5":

				MenuPrincipale.getMenuPrincipale().avvio(calciatoreManager);
				break;
			}

			parametro = null;

		} while (parametro == null);
	}

}
