package it.prismaprogetti.academy.gestionegiocatore.service;

import java.util.Scanner;

import it.prismaprogetti.academy.gestionegiocatore.core.application.manager.CalciatoreManager;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.CalciatoreException;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.NumeroMagliaException;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Cognome;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Nome;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.NumeroMaglia;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Calciatore;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Ruolo;
import it.prismaprogetti.academy.utility.GestoreNumeri;

public class MenuModificaCalciatore {

	private static MenuModificaCalciatore menuModificaCalciatore = null;

	private MenuModificaCalciatore() {
		super();
	}

	public static MenuModificaCalciatore getMenuModificaCalciatore() {

		if (menuModificaCalciatore == null) {
			menuModificaCalciatore = new MenuModificaCalciatore();
		}
		return menuModificaCalciatore;
	}

	public void avvio(Scanner input, CalciatoreManager calciatoreManager) {

		String parametro = null;
		Calciatore calciatore = null;

		/*
		 * recupera calciatore tramite codice
		 */
		do {

			System.out.println("Menu Modifica Calciatore tramite Codice");
			System.out.println("inserisci codice Calciatore oppure N per tornare indietro");

			switch (parametro = input.nextLine().trim().toUpperCase()) {
			case "N":

				parametro = "N";
				break;

			default:

				try {
					calciatore = calciatoreManager.recuperaTramiteCodice(parametro);
					System.out.println("hai selezionato: " + calciatore);
				} catch (CalciatoreException e) {
					parametro = null;
				}

				break;
			}

		} while (parametro == null);

		modificaCalciatore(input, calciatore, calciatoreManager);

	}

	public void modificaCalciatore(Scanner input, Calciatore calciatore, CalciatoreManager calciatoreManager) {

		String parametro = null;
		do {

			System.out.println("Menu modifica calciatore");
			System.out.println("1) Modifica nome");
			System.out.println("2) Modifica cognome");
			System.out.println("3) Modifica ruolo");
			System.out.println("4) Modifica maglia");
			System.out.println("N) Men� precedente");

			switch (parametro = input.nextLine().trim().toUpperCase()) {
			case "1":

				Nome nome = null;
				do {
					System.out.println("nome attuale: " + calciatore.getNome().getNome());
					System.out.println("inserisci nuovo nome o N per tornare indietro");

					switch (parametro = input.nextLine().trim().toUpperCase()) {
					case "N":
						MenuModificaCalciatore.getMenuModificaCalciatore().modificaCalciatore(input, calciatore,
								calciatoreManager);
						break;

					default:

						try {
							nome = Nome.creaOrNull(parametro);
							calciatoreManager.modifica(calciatore, nome, null, null, null);
						} catch (CalciatoreException | NumeroMagliaException | NullPointerException e) {
						}
						break;
					}

				} while (nome == null);

				break;

			case "2":

				Cognome cognome = null;
				do {
					System.out.println("cognome attuale: " + calciatore.getCognome().getCognome());
					System.out.println("inserisci nuovo cognome o N per tornare indietro");

					switch (parametro = input.nextLine().trim().toUpperCase()) {
					case "N":
						MenuModificaCalciatore.getMenuModificaCalciatore().modificaCalciatore(input, calciatore,
								calciatoreManager);
						break;

					default:
						cognome = Cognome.creaOrNull(parametro);
						try {
							calciatoreManager.modifica(calciatore, null, cognome, null, null);
						} catch (CalciatoreException | NumeroMagliaException | NullPointerException e) {
						}
						break;
					}

				} while (cognome == null);

				break;

			case "3":

				Ruolo ruolo = null;
				do {
					System.out.println("ruolo attuale: " + calciatore.getRuolo().toString());
					System.out.println("inserisci nuovo ruolo o N per tornare indietro");

					switch (parametro = input.nextLine().trim().toUpperCase()) {
					case "N":
						MenuModificaCalciatore.getMenuModificaCalciatore().modificaCalciatore(input, calciatore,
								calciatoreManager);
						break;

					default:

						try {
							ruolo = Ruolo.restituisciOrNull(parametro);
							calciatoreManager.modifica(calciatore, null, null, null, ruolo);
							System.out
									.println(calciatoreManager.recuperaTramiteCodice(calciatore.getCodice().getCodice())
											+ " ruolo aggiornato");
						} catch (CalciatoreException | NumeroMagliaException | NullPointerException e) {
						}
						break;
					}

				} while (ruolo == null);

				break;

			case "4":

				NumeroMaglia numeroMaglia = null;
				do {
					System.out.println("numero maglia attuale: " + calciatore.getNumeroMaglia().getNumero());
					System.out.println("inserisci nuovo maglia o N per tornare indietro");

					switch (parametro = input.nextLine().trim().toUpperCase()) {
					case "N":
						MenuModificaCalciatore.getMenuModificaCalciatore().modificaCalciatore(input, calciatore,
								calciatoreManager);
						break;

					default:

						try {
							numeroMaglia = NumeroMaglia.creaOrNull(GestoreNumeri.getNumberFromStringOrZero(parametro));
							calciatoreManager.modifica(calciatore, null, null, numeroMaglia, null);
							System.out
									.println(calciatoreManager.recuperaTramiteCodice(calciatore.getCodice().getCodice())
											+ " numero maglia aggiornato");
						} catch (CalciatoreException | NumeroMagliaException | NullPointerException e) {
						}
						break;
					}

				} while (numeroMaglia == null);

				break;

			case "N":

				MenuGestioneCalciatore.getMenuGestioneCalciatore().avvio(input, calciatoreManager);
				break;

			default:
				parametro = null;
				break;
			}

		} while (parametro == null);

	}

}
