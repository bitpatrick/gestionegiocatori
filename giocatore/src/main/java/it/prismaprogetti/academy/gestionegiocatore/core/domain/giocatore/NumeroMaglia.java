package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore;

import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.NumeroMagliaException;
import it.prismaprogetti.academy.utility.GestoreNumeri;

public class NumeroMaglia {

	private int numero;
	public static final int numeroMassimo = 100;
	private static final int numeroMinimo = 1;

	private NumeroMaglia(int numero) {
		super();
		this.numero = numero;
	}

	public int getNumero() {
		return numero;
	}

	public static NumeroMaglia crea(int numero) throws NumeroMagliaException  {

		if (!GestoreNumeri.numeroInIntervallo(numeroMinimo, numeroMassimo, numero)) {
			throw NumeroMagliaException.numeroMagliaNonValido();
		}

		return new NumeroMaglia(numero);

	}

	public static NumeroMaglia creaOrNull(int numero) {

		if (!GestoreNumeri.numeroInIntervallo(numeroMinimo, numeroMassimo, numero)) {
			return null;
		}

		return new NumeroMaglia(numero);

	}

	public static void verificaCorrettezzaNumeroMaglia(NumeroMaglia numeroMaglia) throws NumeroMagliaException  {

		if (GestoreNumeri.numeroInIntervallo(numeroMinimo, numeroMassimo, numeroMaglia.getNumero())) {
			throw NumeroMagliaException.numeroMagliaNonValido();
		}

	}

	@Override
	public int hashCode() {
		return this.numero;
	}

	@Override
	public boolean equals(Object obj) {
		if ( obj == null ) {
			return false;
		}
		if ( obj instanceof NumeroMaglia ) {
			NumeroMaglia numeroMaglia = (NumeroMaglia) obj;
			return numeroMaglia.getNumero() == this.getNumero();
		}
		return false;
	}
	
	

}
