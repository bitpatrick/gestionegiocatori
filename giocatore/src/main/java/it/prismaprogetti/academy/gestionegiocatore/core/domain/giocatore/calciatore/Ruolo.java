package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore;

public enum Ruolo {

	P("Portiere"), DS("Difensore Sinistro"), DC("Difensore Centrale"), DD("Difensore Destro"),
	CS("Centrocampista Sinistro"), CC("Centrocampista Centrale"), CD("Centrocampista Destro"),
	AS("Attaccante Sinistro"), AC("Attaccante Centrale"), AD("Attaccante Destro");

	private String descrizione;

	private Ruolo(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public static Ruolo restituisci(String ruolo) {

		Ruolo ruoloDaRestituire = Ruolo.valueOf(ruolo);

		if (ruoloDaRestituire == null) {
			throw new NullPointerException("Inserisci un ruolo valido");
		}

		return ruoloDaRestituire;

	}
	
	public static Ruolo restituisciOrNull(String ruolo) {
		
		Ruolo ruoloDaRestituire;
		try {
			ruoloDaRestituire = Ruolo.valueOf(ruolo);
		} catch (IllegalArgumentException e) {
			return null;
		}
		
		return ruoloDaRestituire;
		
	}

}
