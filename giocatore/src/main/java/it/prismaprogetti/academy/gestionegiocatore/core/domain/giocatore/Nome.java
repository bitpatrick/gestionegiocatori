package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore;

import it.prismaprogetti.academy.utility.GestoreStringhe;

public class Nome {

	private String nome;

	private Nome(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public static Nome crea(String nome) {

		if (!GestoreStringhe.soloCaratteri(nome)) {
			throw new IllegalStateException("Nome invalido");
		}
		String nomeDaCreare = GestoreStringhe.correggiFormatoStringa(nome);

		return new Nome(nomeDaCreare);
	}

	public static Nome creaOrNull(String nome) {

		if (!GestoreStringhe.soloCaratteri(nome)) {
			return null;
		}
		String nomeDaCreare = GestoreStringhe.correggiFormatoStringa(nome);

		return new Nome(nomeDaCreare);
	}

	public static boolean isNull(Nome nomeCalciatore) {

		String nome = nomeCalciatore.getNome();

		if (nome == null) {
			return true;
		}

		return false;

	}

}
