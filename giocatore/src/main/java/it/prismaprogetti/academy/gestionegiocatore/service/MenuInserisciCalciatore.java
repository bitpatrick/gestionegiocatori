package it.prismaprogetti.academy.gestionegiocatore.service;

import java.util.Scanner;

import it.prismaprogetti.academy.gestionegiocatore.core.application.manager.CalciatoreManager;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.CalciatoreException;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.NumeroMagliaException;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Cognome;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Nome;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.NumeroMaglia;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Calciatore;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Ruolo;
import it.prismaprogetti.academy.utility.GestoreNumeri;

public class MenuInserisciCalciatore {

	private static MenuInserisciCalciatore menuInserisciCalciatore;

	private MenuInserisciCalciatore() {
		super();
	}

	public static MenuInserisciCalciatore getMenuInserisciCalciatore() {

		if (menuInserisciCalciatore == null) {
			menuInserisciCalciatore = new MenuInserisciCalciatore();
		}
		return menuInserisciCalciatore;
	}

	public void inserisciCalciatore(Scanner input, CalciatoreManager calciatoreManager) {

		Calciatore calciatore = null;
		Nome nome;
		Cognome cognome;
		NumeroMaglia numeroMaglia;
		Ruolo ruolo;
		/*
		 * imposta nome
		 */
		do {
			System.out.println("inserisci nome");
			nome = Nome.creaOrNull(input.nextLine());
		} while (nome == null);

		/*
		 * imposto cognome
		 */
		do {
			System.out.println("inserisci cognome");
			cognome = Cognome.creaOrNull(input.nextLine());
		} while (cognome == null);

		/*
		 * inserisci ruolo
		 */
		do {
			System.out.println("inserisci ruolo");
			ruolo = Ruolo.restituisciOrNull(input.nextLine().trim().toUpperCase());
		} while (ruolo == null);

		/*
		 * imposta numero maglia
		 */
		do {
			System.out.println("inserisci numero di maglia");
			int numero = GestoreNumeri.getNumberFromStringOrZero(input.nextLine());
			numeroMaglia = NumeroMaglia.creaOrNull(numero);

		} while (numeroMaglia == null);

		/*
		 * creo il calciatore con i suddetti parametri e tento di inserirlo nel
		 * repository
		 */
		calciatore = Calciatore.crea(nome, cognome, numeroMaglia, ruolo);
		try {
			calciatoreManager.inserisciNelRepository(calciatore);
		} catch (CalciatoreException | NumeroMagliaException e) {
			System.err.println(e.getMessage());
		}
	}

}
