package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore;

import java.util.List;
import java.util.Optional;

import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Codice;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.NumeroMaglia;

public interface CalciatoreRepository {
	
	/*
	 * create
	 */
	void insert(Calciatore calciatore);
	
	/*
	 * read
	 */
	Optional<Calciatore> getByCodice(String codice);
	Optional<Calciatore> getByNumeroMaglia(NumeroMaglia numeroMaglia);
	List<Calciatore> getByRuolo(Ruolo ruolo);
	List<Calciatore> getAll();
	
	/*
	 * update
	 */
	void update(Calciatore calciatore);
	
	/*
	 * delete
	 */
	void delete(Calciatore calciatore);

}
