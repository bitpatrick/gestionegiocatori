package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore;

public enum Tipo {

	CALCIATORE, TENNISTA;
	
}
