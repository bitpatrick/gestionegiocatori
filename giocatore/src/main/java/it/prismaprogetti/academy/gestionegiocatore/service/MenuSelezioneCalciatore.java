package it.prismaprogetti.academy.gestionegiocatore.service;

import java.util.List;
import java.util.Scanner;

import it.prismaprogetti.academy.gestionegiocatore.core.application.manager.CalciatoreManager;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.exception.CalciatoreException;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Calciatore;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.Ruolo;

public class MenuSelezioneCalciatore {

	private static MenuSelezioneCalciatore menuSelezioneCalciatore;

	private MenuSelezioneCalciatore() {
		super();
	}

	public static MenuSelezioneCalciatore getMenuSelezioneCalciatore() {

		if (menuSelezioneCalciatore == null) {
			menuSelezioneCalciatore = new MenuSelezioneCalciatore();
		}

		return menuSelezioneCalciatore;

	}

	public void avvio(Scanner input, CalciatoreManager calciatoreManager) {

		String parametro = null;

		do {

			System.out.println("Menu Selezione Calciatore");
			System.out.println("1) per codice");
			System.out.println("2) per ruolo");
			System.out.println("3) per tutti i calciatori");
			System.out.println("N) per tornare al menu precedente");

			switch (parametro = input.nextLine().trim().toUpperCase()) {
			case "1":
				Calciatore calciatore = null;
				do {
					System.out.println("inserisci codice o N per tornare indietro");

					switch (parametro = input.nextLine().trim().toUpperCase()) {
					case "N":
						MenuSelezioneCalciatore.getMenuSelezioneCalciatore().avvio(input, calciatoreManager);

						break;

					default:
						try {
							calciatore = calciatoreManager.recuperaTramiteCodice(parametro);
						} catch (CalciatoreException e) {
							System.err.println(e.getMessage());
						}

						break;
					}

				} while (calciatore == null);
				System.out.println(calciatore + " recuperato tramite codice");

				break;
			case "2":

				List<Calciatore> calciatoriByRuoli = null;
				do {
					System.out.println("inserisci ruolo o N per tornare indietro");
					switch (parametro = input.nextLine().trim().toUpperCase()) {
					case "N":
						MenuSelezioneCalciatore.getMenuSelezioneCalciatore().avvio(input, calciatoreManager);

						break;

					default:
						try {
							Ruolo ruolo = Ruolo.restituisciOrNull(parametro);
							calciatoriByRuoli = calciatoreManager.recuperaCalciatoriTramiteRuolo(ruolo);
						} catch (CalciatoreException e) {

							System.err.println(e.getMessage());
						}

						break;
					}

				} while (calciatoriByRuoli == null);

				System.out.println("Calciatori recuperati tramite ruolo:");
				for (Calciatore calciatoreByRuolo : calciatoriByRuoli) {
					System.out.println(calciatoreByRuolo);
				}

				break;
				
			case "3":
				
				List<Calciatore> calciatori = calciatoreManager.recuperaCalciatori();
				for (Calciatore c : calciatori) {
					System.out.println(c);
				}
				
				break;

			case "N":

				MenuGestioneCalciatore.getMenuGestioneCalciatore().avvio(input, calciatoreManager);

				break;

			default:

				System.err.println("Hai inserito un parametro non valido");
				parametro = null;
				break;
			}

		} while (parametro == null);

	}

}
