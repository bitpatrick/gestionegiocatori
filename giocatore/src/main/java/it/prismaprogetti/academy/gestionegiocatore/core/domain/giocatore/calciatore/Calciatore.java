package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore;

import static it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Tipo.CALCIATORE;

import java.util.List;

import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Codice;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Cognome;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Giocatore;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.Nome;
import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.NumeroMaglia;

public class Calciatore extends Giocatore {

	private static int numeroCalciatoriGenerati = 0;
	private Ruolo ruolo;

	private Calciatore(Codice codice, Nome nome, Cognome cognome, NumeroMaglia numeroMaglia, Ruolo ruolo) {
		super(codice, nome, cognome, numeroMaglia, CALCIATORE);
		this.ruolo = ruolo;
	}

	public static int getNumeroCalciatoriGenerati() {
		return numeroCalciatoriGenerati;
	}

	public Ruolo getRuolo() {
		return ruolo;
	}

	public void setRuolo(Ruolo ruolo) {
		this.ruolo = ruolo;
	}

	public static Calciatore crea(Nome nome, Cognome cognome, NumeroMaglia numeroMaglia, Ruolo ruolo) {

		/*
		 * creazione codice calciatore sequenziale
		 */
		Codice codiceCalciatoreDaCreare = Codice.creaCodice(CALCIATORE);
		numeroCalciatoriGenerati += 1;

		return new Calciatore(codiceCalciatoreDaCreare, nome, cognome, numeroMaglia, ruolo);

	}

	public static boolean isCalciatore(Object object) {
		if (object instanceof Calciatore) {
			return true;
		}
		return false;
	}

	public static boolean isElencoCalciatori(Object object) {

		List<Calciatore> calciatori = null;
		try {
			calciatori = (List<Calciatore>) object;
		} catch (ClassCastException e) {
		}

		if (calciatori == null) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "Calciatore [" + "codice: " + getCodice().getCodice() + " nome: " + getNome().getNome() + " cognome: "
				+ getCognome().getCognome() + " numero maglia: " + getNumeroMaglia().getNumero() + " ruolo= " + ruolo.getDescrizione()
				+ "]";
	}

}
