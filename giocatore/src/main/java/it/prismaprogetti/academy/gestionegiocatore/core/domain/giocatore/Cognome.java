package it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore;

import it.prismaprogetti.academy.utility.GestoreStringhe;

public class Cognome {

	private String cognome;

	private Cognome(String cognome) {
		super();
		this.cognome = cognome;
	}

	public String getCognome() {
		return cognome;
	}

	public static Cognome crea(String cognome) {

		if (!GestoreStringhe.soloCaratteri(cognome)) {
			throw new IllegalStateException("Cognome invalido");
		}
		String cognomeDaCreare = GestoreStringhe.correggiFormatoStringa(cognome);

		return new Cognome(cognomeDaCreare);

	}

	public static Cognome creaOrNull(String cognome) {

		if (!GestoreStringhe.soloCaratteri(cognome)) {
			return null;
		}
		String cognomeDaCreare = GestoreStringhe.correggiFormatoStringa(cognome);
		
		return new Cognome(cognomeDaCreare);
	}

}
