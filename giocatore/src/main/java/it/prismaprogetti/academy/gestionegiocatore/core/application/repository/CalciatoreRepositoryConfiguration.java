package it.prismaprogetti.academy.gestionegiocatore.core.application.repository;

import it.prismaprogetti.academy.gestionegiocatore.core.domain.giocatore.calciatore.CalciatoreRepository;
import it.prismaprogetti.academy.impl.InMemoryCalciatoreRepository;

public class CalciatoreRepositoryConfiguration {

	private static CalciatoreRepository calciatoreRepository;

	private CalciatoreRepositoryConfiguration() {
		super();

	}

	/*
	 * memoria ram
	 */
	public static CalciatoreRepository getInMemoryRepository() {

		if (calciatoreRepository == null) {
			calciatoreRepository = new InMemoryCalciatoreRepository();

		}
		return calciatoreRepository;
	}

}
