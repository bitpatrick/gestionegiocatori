package it.prismaprogetti.academy.gestionegiocatore.core.domain.exception;

public class CalciatoreException extends Exception {

	private CalciatoreException(String message) {
		super(message);
	}

	public static CalciatoreException calciatoreGi�Esistente() {
		return new CalciatoreException("Calciatore gi� esistente");
	}

	public static CalciatoreException calciatoreNonPresente() {
		return new CalciatoreException("Impossibile recuperare il calciatore desiderato");
	}
	
	public static CalciatoreException calciatoriNonPresenti() {
		return new CalciatoreException("Impossibile recuperare i calciatori desiderati");
	}

}
