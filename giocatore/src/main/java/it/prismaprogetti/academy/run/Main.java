package it.prismaprogetti.academy.run;

import it.prismaprogetti.academy.gestionegiocatore.core.application.manager.CalciatoreManager;
import it.prismaprogetti.academy.gestionegiocatore.core.application.repository.CalciatoreRepositoryConfiguration;
import it.prismaprogetti.academy.gestionegiocatore.service.MenuPrincipale;

public class Main {

	public static void main(String[] args) {

		/*
		 * richiedo menu principale
		 */
		MenuPrincipale menuPrincipale = MenuPrincipale.getMenuPrincipale();

		/*
		 * avvio il menu principale impostando un gestore dei calciatori che a sua volta 
		 * dovr� aver impostato un repository su cui lavorare:
		 * il repository passato � in memoria di tipo singleton
		 */
		menuPrincipale.avvio(new CalciatoreManager( CalciatoreRepositoryConfiguration.getInMemoryRepository() ));

	}

}
